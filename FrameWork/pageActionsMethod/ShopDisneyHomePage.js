const utilites = require('../helpers/utils')
const ShopDisneyHomePageLocators = require('../pageLocators/ShopDisneyHomePageLocators.js')
const wait = require('../helpers/constants.js')
const wdio = require('webdriverio')
const { MEDIUMWAIT } = require('../helpers/constants.js')

class ShopDisneyHomePage  {
      /**
       * Purpose- Launch the ShopDisney url
       * @param {*} Url
       */
      launchURL(Url){
         utilites.launchURL(Url)
      }
      /**
       * Purpose-Verify ShopDisney Home Page
       */
      verifyHomePage(){
        utilites.waitForVisible(ShopDisneyHomePageLocators.VIRTUALASSISTANT, wait.LONGWAIT, "'Virtual Assistant'{chatbot} in Home page")
        utilites.waitForVisible(ShopDisneyHomePageLocators.SHOPDISNEY_LOGO, wait.MEDIUMWAIT, "'Shop Disney' Logo in Home page")
    }

    /**
     *  Purpose-Click on Virtual Assistant{chatbot}
     */
    clickOnVirtualAssistant(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.VIRTUALASSISTANT,wait.MEDIUMWAIT, "'Virtual Assistant'{chatbot} in Home page")
        utilites.safePause(wait.VERYSHORTWAIT,"To load the Virtual Assistant{chatbot}")
    }
    /**
     * Purpose-Verify virtual assistant{chatbot} is opened or not
     */
    verifyVirtualAssistant(){
        utilites.switchToFrame(ShopDisneyHomePageLocators.VIRTUALASSISTANT_IFRAME, "'Virtual Assistant'{chartbot}")
        utilites.waitForVisible(ShopDisneyHomePageLocators.VIRTUALASSISTANT_HEADER, wait.LONGWAIT, "'Virtual Assistant'{chatbot} Header")
        utilites.waitForVisible(ShopDisneyHomePageLocators.ORDERSTATUS_OPTION, wait.MEDIUMWAIT, "'Order Status' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.SOMETHINGELSE_OPTION, wait.MEDIUMWAIT, "'Something else' option")
        
    }
    /**
     * Purpose-Select 'Order Status' option in chatbot & Verify the following
     * A) 'Glad to help! Do you have your order number and the email address associated with this order?' text is displayed or not.
     * B) 'Yes', 'No' and 'Go Back' options are displayed or not.
     */
    clickOnOrderStatus(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.ORDERSTATUS_OPTION,wait.MEDIUMWAIT, "'Order Status' option")
        utilites.safePause(wait.VERYVERYSHORTWAIT,"To load the new interaction text in chatbot")
        utilites.waitForVisible(ShopDisneyHomePageLocators.ORDERNUMBERANDEMAILASSOCIATED_TEXT, wait.MEDIUMWAIT, "''Glad to help! Do you have your order number and the email address associated with this order?'' text in chatbot")
        utilites.waitForVisible(ShopDisneyHomePageLocators.YES_OPTION, wait.MEDIUMWAIT, "'Yes' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.NO_OPTION, wait.MEDIUMWAIT, "'No' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.GOBACK_OPTION, wait.MEDIUMWAIT, "'Go Back' option")
    }
    /**
     * Purpose-Click on 'No' option & Verify the 'Locate Order Number' and 'No Email Address' options are displayed or not.
     *  
     */
    clickOnNoOption(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.NO_OPTION, wait.MEDIUMWAIT, "'No' option in chatbot")
        utilites.waitForVisible(ShopDisneyHomePageLocators.NOEMAIlADDRESS_OPTION, wait.MEDIUMWAIT, "'No Email Address' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.LOCATEORDERNUMBER_OPTION, wait.MEDIUMWAIT, "'Locate Order Number'")
    }
    /**
     * Purpose-Click on 'Locate Order Number' opton & verify the preview with text 'How to find my Order Number' is displayed or not.
     */
    clickOnLocateOrderNumber(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.LOCATEORDERNUMBER_OPTION, wait.MEDIUMWAIT, "'Locate Order Number' option in chatbot")
        utilites.waitForVisible(ShopDisneyHomePageLocators.FINDMYORDERNUMBER_PREVIEW_IFRAME, wait.MEDIUMWAIT, "'How to find my order number' frame")
        utilites.switchToFrame(ShopDisneyHomePageLocators.FINDMYORDERNUMBER_PREVIEW_IFRAME, "'How to find my order number'")
        utilites.safePause(wait.VERYVERYSHORTWAIT, "To load the preview with text 'How to find my Order Number'")
        utilites.waitForVisible(ShopDisneyHomePageLocators.HOWTOFINDMYORDERNUMBER_PREVIEW, wait.MEDIUMWAIT, "Preview with text 'How to find my order number'")
        utilites.switchToParentFrame();
    }
    /**
     * Purpose-Close the preview with text 'How to find my order number' & verify the following
     * A) 'Was it helpful' text is displayed or not.
     * B) 'Yes, I found my order number' and 'No, I could not find order email' options are displayed or not.
     */
    clickOnCloseIconInHowtoFindMyOrderNumberPreview(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.CLOSEICON_PREVIEW, wait.MEDIUMWAIT, "'Close' icon in the preview with text 'How to find my order number'")
        utilites.waitForVisible(ShopDisneyHomePageLocators.WASITHELPFUL_TEXT, wait.MEDIUMWAIT, "'Was it Helpful' text")
        utilites.waitForVisible(ShopDisneyHomePageLocators.YESIFOUNDMYORDERNUMBER_OPTION, wait.MEDIUMWAIT, "'Yes, I found my order number' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.NOICOUNDNOTFINDORDEREMAIL_OPTION, wait.MEDIUMWAIT, "'No, I could not find order email' option")

    }
    /**
     * Purpose - Click on 'Yes, I found my order number' option & verify the following 
     * A) 'Great. Do you want me to try looking up your order details?' text is displayed or not.
     * B) 'Yes' and 'No' options are displayed or not.
     */
    clickOnYesIFoundMyOrderNumber(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.YESIFOUNDMYORDERNUMBER_OPTION, wait.MEDIUMWAIT, "'Yes, I found my order number' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.ORDERDETAILS_YES_OPTION, wait.MEDIUMWAIT, "'Yes' Option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.ORDERDETAILS_NO_OPTION, wait.MEDIUMWAIT, "'No' Option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.TRYLOOKINGUPYOURORDERDETAILS_TEXT, wait.MEDIUMWAIT, "'Great. Do you want me to try looking up your order details?' text")
    }
    /**
     * Purpose- Click on 'Yes' option & Verify the text 'Please provide your Order Number' text is displayed or not.
     */
    clickOnYesOption(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.ORDERDETAILS_YES_OPTION, wait.MEDIUMWAIT, "'Yes' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.PLEASEPROVIDEYOURORDERNUMBER_TEXT, wait.MEDIUMWAIT, "'Please provide your Order Number' text")
    }
    /**
     * Purpose-Enter Order Details
     * @param {*} orderNumber 
     * @param {*} emailAddress
     */
    enterOrderDetails(orderNumber, emailAddress){
        utilites.clearElement(ShopDisneyHomePageLocators.DESCRIPTION_TEXTFIELD, "'Description' textfield")
        utilites.safeSetValue(ShopDisneyHomePageLocators.DESCRIPTION_TEXTFIELD, orderNumber, wait.MEDIUMWAIT, "'Description' textfield")
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.SENDMESSAGE_ICON, wait.MEDIUMWAIT, "'Send Message' icon next to 'Description' textfield")
        utilites.waitForVisible(ShopDisneyHomePageLocators.EMAILADDRESSASSOCIATEDWITHTHISORDER_TEXT, wait.MEDIUMWAIT, "'Thanks. And what is the Email Address associated with this order?' text")
        utilites.clearElement(ShopDisneyHomePageLocators.DESCRIPTION_TEXTFIELD, "'Description' textfield")
        utilites.safeSetValue(ShopDisneyHomePageLocators.DESCRIPTION_TEXTFIELD, emailAddress, wait.MEDIUMWAIT, "'Description' textfield")
        
    }
    /**
     * Purpose-Click on 'send message' icon & verify the following
     * A) 'I am not able to find an order for the Order Number and Email Address you provided.' text is displayed or not.
     * B) 'Yes' and 'No' options are displayed or not.
     */
    clickOnSendMessageIcon(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.SENDMESSAGE_ICON, wait.MEDIUMWAIT, "'Send Message' icon next to 'Description' textfield")
        utilites.waitForVisible(ShopDisneyHomePageLocators.NOTABLETOFINDANORDERNUMBERANDEMAIL_TEXT, wait.MEDIUMWAIT, "'I am not able to find an order for the Order Number and Email Address you provided.' text")
        utilites.waitForVisible(ShopDisneyHomePageLocators.CHECKANOTHEREMAIL_YES_OPTION, wait.MEDIUMWAIT, "'Yes' Option - Check with another email")
        utilites.waitForVisible(ShopDisneyHomePageLocators.CHECKANOTHEREMAIL_NO_OPTION, wait.MEDIUMWAIT, "'No' option - Check with another email")
    }
    /**
     * Purpose-Click on 'No' Option & Verify the following
     * A) 'No problem. Let me connect you with a cast member for further help.' text is displayed or not.
     * B) 'Chat with Cast Member' option is displayed or not.
     */
    clickOnNoOptiontoCheckWithAnotherEmail(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.CHECKANOTHEREMAIL_NO_OPTION, wait,MEDIUMWAIT, "'No' option - Check with another email")
        utilites.waitForVisible(ShopDisneyHomePageLocators.CONNECTWITHCASTMEMBER_TEXT, wait.MEDIUMWAIT,"''No problem. Let me connect you with a cast member for further help.' text")
        utilites.waitForVisible(ShopDisneyHomePageLocators.CHATWITHCASTMEMBER_OPTION, wait.MEDIUMWAIT, "'Chat with Cast Member' option")
    }
    /**
     * Purpose-Enter 'Track my order' text in the 'Description' text field
     * @param {*} conversation_text
     */
    enterText(conversation_text){
        utilites.clearElement(ShopDisneyHomePageLocators.DESCRIPTION_TEXTFIELD, "'Description' textfield")
        utilites.safeSetValue(ShopDisneyHomePageLocators.DESCRIPTION_TEXTFIELD, conversation_text, wait.MEDIUMWAIT, "'Track my order' text in 'Description' textfield")
        
    }
    /**
     * Purpose-Hit Enter & verify the following
     * A) 'Glad to help! Do you have your order number and the email address associated with this order?' text is displayed or not.
     * B) 'Yes' and 'No' options are displayed or not.
     */
    sendKeys_Enter(){
        utilites.browserkeys('Enter', "In chatbot after entering text in 'description' textfield")  
        utilites.waitForVisible(ShopDisneyHomePageLocators.ORDERNUMBERANDEMAILASSOCIATED_TEXT, wait.MEDIUMWAIT, "''Glad to help! Do you have your order number and the email address associated with this order?'' text in chatbot")
        utilites.waitForVisible(ShopDisneyHomePageLocators.YES_OPTION, wait.MEDIUMWAIT, "'Yes' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.NO_OPTION, wait.MEDIUMWAIT, "'No' option")
        
    }
    /**
     * Purpose- Click on 'Yes' option for Order Details & Verify the text 'Please provide your Order Number' text is displayed or not.
     */
     clickOnYesOptionForOrderDetails(){
        utilites.safeVisibleClick(ShopDisneyHomePageLocators.YES_OPTION, wait.MEDIUMWAIT, "'Yes' option")
        utilites.waitForVisible(ShopDisneyHomePageLocators.PLEASEPROVIDEYOURORDERNUMBER_TEXT, wait.MEDIUMWAIT, "'Please provide your Order Number' text")
    }


}

module.exports = new ShopDisneyHomePage();