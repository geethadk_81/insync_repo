/**
 * Timeout used in scripts to wait for specified interval
 **/
//export const DEFAULT_TIMEOUT = 11000;
module.exports = {
    "MEDIUMWAIT": 10000,
    "SHORTWAIT": 9500,
    "VERYSHORTWAIT": 6500,
    "VERYVERYSHORTWAIT": 2000,
    "LONGWAIT": 25000,
    "VERYLONGWAIT": 35000,
    "VERYVERYLONGWAIT": 70000
}