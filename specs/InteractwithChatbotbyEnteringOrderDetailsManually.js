//Test data
const TestData = require('../testdata/globalData.json')
const ShopDisneyHomepage = require('../FrameWork/pageActionsMethod/ShopDisneyHomePage.js')

/**
 *TestCase- Interacting with chatbot feature by entering Order Details text manually
 */
describe("InteractingwithChatbotFeatureinShopDisney", function() 
{

    it("TC002_EnteringOrderDetailstextManually", function()
    {
        //Launch the ShopDisney application
        ShopDisneyHomepage.launchURL(TestData.URL.ShopDisney_url);

        //Verify the Shop Disney Home page
        ShopDisneyHomepage.verifyHomePage();

        //Click on Virtual Assistant icon {chatbot} in Home page
        ShopDisneyHomepage.clickOnVirtualAssistant();

        //Verify virtual assistant{chatbot} is opened or not
        ShopDisneyHomepage.verifyVirtualAssistant();

        //Enter 'Track my order' text in the 'Description' text field
        ShopDisneyHomepage.enterText(TestData.Data.Conversation_text);

        /*
        Hit Enter & verify the following
        Expected Result - User should be able to see the following
        A) 'Glad to help! Do you have your order number and the email address associated with this order?' text is displayed or not.
        B) 'Yes' and 'No' options are displayed or not.
        */
       ShopDisneyHomepage.sendKeys_Enter();

        //Click on 'Yes' option & Verify the text 'Please provide your Order Number' text is displayed or not.
        ShopDisneyHomepage.clickOnYesOptionForOrderDetails();

        //Enter Order Details
        ShopDisneyHomepage.enterOrderDetails(TestData.Data.OrderNumber, TestData.Data.EmailAddress);

        /*
        Purpose-Click on 'send message' icon
        Expected Result - User should be able to see the following
        A) 'I am not able to find an order for the Order Number and Email Address you provided.' text is displayed or not.
        B) 'Yes' and 'No' options are displayed or not.
        */
        ShopDisneyHomepage.clickOnSendMessageIcon();

        /*
        Click on 'No' Option
        Expected Result - User should be able to see the following
        A) 'No problem. Let me connect you with a cast member for further help.' text is displayed or not.
        B) 'Chat with Cast Member' option is displayed or not.
        */
        ShopDisneyHomepage.clickOnNoOptiontoCheckWithAnotherEmail();
    })
})